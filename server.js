var finalhandler    = require('finalhandler');
var http            = require('http');
var serveStatic     = require('serve-static');

var port = 3000;

var serve = serveStatic('./', {'index': ['index.html', 'index.htm']});

var server = http.createServer(function(req, res){
	var done = finalhandler(req, res);
	serve(req, res, done);
});

server.listen(port);
console.log('Server listening port', port);