var browserify  = require('browserify');
var stringify   = require('stringify');
var through     = require('through');
var fs          = require('fs');
var path        = require('path');

try {
	var b = browserify();
	b.add(path.join(__dirname, './js/simpleAudioPlayer/src/simpleAudioPlayer.js'));
	var content = '';

	b.transform(stringify(['.json', '.txt', '.html']));
	b.bundle().pipe(through(function(data) {
		content += data.toString('utf8');
	}, function() {
		fs.writeFileSync('./js/simpleAudioPlayer/build/simpleAudioPlayer.build.js', content)
	}));
}
catch (error) {
	console.error(error);
}
