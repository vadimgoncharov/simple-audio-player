'use strict';

/**
 * @module simpleAudioPlayer/components/ko/muteButton/muteButtonViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function MuteButtonViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.isMute = this.playerViewModel.isMute;
}

MuteButtonViewModel.prototype = {
	constructor: MuteButtonViewModel,

	toggleMute: function() {
		this.isMute(!this.isMute());
	}
};

module.exports = MuteButtonViewModel;