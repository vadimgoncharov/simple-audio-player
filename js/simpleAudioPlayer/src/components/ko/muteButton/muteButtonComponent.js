'use strict';

var MuteButtonTemplate     = require('./muteButtonTpl.html');
var MuteButtonViewModel    = require('./muteButtonViewModel');

function MuteButtonComponent() {}

MuteButtonComponent.prototype = {
	constructor: MuteButtonComponent,
	componentName: 'mute-button',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: MuteButtonViewModel,
				template: MuteButtonTemplate
			});
		}
	}
};

module.exports = new MuteButtonComponent();