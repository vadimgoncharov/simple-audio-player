'use strict';

var TimeCounterTemplate     = require('./timeCounterTpl.html');
var TimeCounterViewModel    = require('./timeCounterViewModel');

function TimelineComponent() {}

TimelineComponent.prototype = {
	constructor: TimelineComponent,
	componentName: 'time-counter',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: TimeCounterViewModel,
				template: TimeCounterTemplate
			});
		}
	}
};

module.exports = new TimelineComponent();