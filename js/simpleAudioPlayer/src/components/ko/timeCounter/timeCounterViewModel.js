'use strict';

/**
 * @module simpleAudioPlayer/components/ko/timeCounter/timeCounterViewModel
 */

var util = require('../../../services/util');

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function TimeCounterViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.trackCurrTime = this.playerViewModel.trackCurrTime;
	this.trackDuration = this.playerViewModel.trackDuration;

	this.trackCurrTimeInMMSS = ko.computed(function() {
		return util.toMMSS(this.trackCurrTime());
	}, this);

	this.trackDurationInMMSS = ko.computed(function() {
		return util.toMMSS(this.trackDuration());
	}, this);

}

module.exports = TimeCounterViewModel;