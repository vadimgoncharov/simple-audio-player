'use strict';

/**
 * @module simpleAudioPlayer/components/ko/timeLine/timeLineViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function TimeLineViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.trackCurrTime = ko.computed(function() {
		return Math.round(this.playerViewModel.trackCurrTime());
	}, this);

	this.trackDuration = ko.computed(function() {
		return Math.round(this.playerViewModel.trackDuration());
	}, this);
	
	this.playingValueOnStartRangeInput = null;
	this.loadedInPercent = this.playerViewModel.loadedInPercent;

	this.inputWidth = ko.computed(function() {
		return this.loadedInPercent() + '%';
	}, this);

}

TimeLineViewModel.prototype = {
	constructor: TimeLineViewModel,

	onRangeChange: function(data, event) {
		this.playerViewModel.changeCurrTrackTimeTo(event.target.value);

		if (this.playingValueOnStartRangeInput !== null) {
			this.playerViewModel.isPlaying(this.playingValueOnStartRangeInput);
			this.playingValueOnStartRangeInput = null;
		}
	},

	onRangeInput: function(data, event) {
		if (this.playingValueOnStartRangeInput === null) {
			this.playingValueOnStartRangeInput = this.playerViewModel.isPlaying();
		}
		this.playerViewModel.changeCurrTrackTimeTo(event.target.value);
	}
};

module.exports = TimeLineViewModel;