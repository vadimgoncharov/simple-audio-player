'use strict';

var TimeLineTemplate     = require('./timeLineTpl.html');
var TimeLineViewModel    = require('./timeLineViewModel');

function TimelineComponent() {}

TimelineComponent.prototype = {
	constructor: TimelineComponent,
	componentName: 'time-line',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: TimeLineViewModel,
				template: TimeLineTemplate
			});
		}
	}
};

module.exports = new TimelineComponent();