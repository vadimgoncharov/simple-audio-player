'use strict';

var PlayPauseButtonTemplate     = require('./playPauseButtonTpl.html');
var PlayPauseButtonViewModel    = require('./playPauseButtonViewModel');

function PlayPauseButtonComponent() {}

PlayPauseButtonComponent.prototype = {
	constructor: PlayPauseButtonComponent,
	componentName: 'play-pause-button',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: PlayPauseButtonViewModel,
				template: PlayPauseButtonTemplate
			});
		}
	}
};

module.exports = new PlayPauseButtonComponent();