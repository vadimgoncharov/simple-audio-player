'use strict';

/**
 * @module simpleAudioPlayer/components/ko/playPauseButton/playPauseButtonViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function PlayPauseButtonViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.isPlaying = this.playerViewModel.isPlaying;
}

PlayPauseButtonViewModel.prototype = {
	constructor: PlayPauseButtonViewModel,

	togglePlay: function() {
		this.isPlaying(!this.isPlaying());
	}
};

module.exports = PlayPauseButtonViewModel;