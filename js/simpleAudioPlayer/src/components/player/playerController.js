'use strict';

/**
 * This is main app component which creates child components
 * @module simpleAudioPlayer/components/player/playerController
 */

var PlayerViewModel    = require('./playerViewModel');
var PlayerTpl          = require('./playerTpl.html');

var PlayPauseButtonComponent    = require('../ko/playPauseButton/playPauseButtonComponent');
var MuteButtonComponent         = require('../ko/muteButton/muteButtonComponent');
var TimeLineComponent           = require('../ko/timeLine/timeLineComponent');
var TimeCounterComponent        = require('../ko/timeCounter/timeCounterComponent');

var PlayerTrackModel            = require('../playerTrack/playerTrackModel');
var util                        = require('../../services/util');

function PlayerController(rootEl, options) {
	this.rootEl = rootEl;
	this.options = options;

	this.createVerbose();
	this.registerKoComponents();
	this.initPlayer();
	this.renderPlayer();
}

PlayerController.prototype = {
	constructor: PlayerController,

	createVerbose: function() {
		this.verbose = util.createVerboseFunction(this.options.verbose);
	},

	registerKoComponents: function() {
		PlayPauseButtonComponent.register();
		MuteButtonComponent.register();
		TimeLineComponent.register();
		TimeCounterComponent.register();
	},

	initPlayer: function() {
		this.playerViewModel = new PlayerViewModel({
			verbose: this.verbose
		});

		var playerTrackModel = new PlayerTrackModel({
			url: this.options.trackUrl,
			verbose: this.verbose
		});

		this.playerViewModel.setTrack(playerTrackModel);
	},

	renderPlayer: function() {
		var el = document.createElement('div');
		el.innerHTML = PlayerTpl;
		this.rootEl.appendChild(el);

		ko.applyBindings(this.playerViewModel, this.rootEl);
	}
};

module.exports = PlayerController;
