'use strict';

/**
 * @module simpleAudioPlayer/components/player/playerViewModel
 */

/**
 *
 * @param {object} options
 * @param {function} options.verbose
 * @constructor
 */
function PlayerViewModel(options) {
	this.verbose = options.verbose;

	this.playerViewModel = this;
	this.playerTrackModel = null;
	this.trackCurrTime = ko.observable(0);
	this.trackDuration = ko.observable(0);
	this.isPlaying = ko.observable(false);
	this.isMute = ko.observable(false);
	this.loadedInPercent = ko.observable(-1);

	this.isPlaying.subscribe(this.changePlayingByIsPlaying, this);
	this.isMute.subscribe(this.changeMuteByIsMute, this);
	this.trackCurrTime.subscribe(this.stopIfReachEnd, this);
}

PlayerViewModel.prototype = {
	constructor: PlayerViewModel,

	/**
	 * Reset timer to beginning if it reachs the end
	 */
	stopIfReachEnd: function() {
		if (this.trackCurrTime() > this.trackDuration()) {
			this.isPlaying(false);
			this.trackCurrTime(this.trackDuration());
		}
	},

	/**
	 * Change Player playing state by testing isPlaying prop
	 */
	changePlayingByIsPlaying: function() {
		if (this.isPlaying()) {
			this.play();
		}
		else {
			this.stop();
		}
	},

	/**
	 * Change Player mute state by testing isMute prop
	 */
	changeMuteByIsMute: function() {
		if (this.isMute()) {
			this.playerTrackModel.mute();
		}
		else {
			this.playerTrackModel.unmute();
		}
	},

	/**
	 * Set current active track
	 * @param {PlayerTrackModel} playerTrackModel
	 */
	setTrack: function(playerTrackModel) {
		this.playerTrackModel = playerTrackModel;

		this.playerTrackModel.currTime.subscribe(this.trackCurrTime, this);
		this.playerTrackModel.duration.subscribe(this.trackDuration, this);
		this.playerTrackModel.loadedInPercent.subscribe(this.loadedInPercent, this);
	},

	/**
	 * Change current time of current track to new time
	 * @param {number} newTime
	 */
	changeCurrTrackTimeTo: function(newTime) {
		this.isPlaying(false);
		this.playerTrackModel.changeCurrTimeTo(newTime);
	},

	/**
	 * Run audio track
	 */
	play: function() {
		if (this.trackCurrTime() >= this.trackDuration()) {
			this.playerTrackModel.changeCurrTimeTo(0);
		}
		this.playerTrackModel.loadAndDecode()
			.then(function() {
				this.playerTrackModel.play();
				this.changeMuteByIsMute();
			}.bind(this))
			.catch(function(error) {
				console.error(error);
			}
		);
	},

	/**
	 * Stop audio track
	 */
	stop: function() {
		this.playerTrackModel.stop();
	}
};

module.exports = PlayerViewModel;
