'use strict';

/**
 * Load track using XMLHttpRequest and decodes compressedBuffer to buffer
 * @module simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode
 */

var requestService = require('../../services/request');

var getAudioContext = (function() {
	var AudioContext;

	return function() {
		if (!AudioContext) {
			try {
				AudioContext = window.AudioContext || window.webkitAudioContext;
			}
			catch (error) {
				throw new Error('Web Audio API is not supported in this browser');
			}
		}

		return new AudioContext;
	}
})();

/**
 * @typedef module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options
 * @type {object} options
 * @property {string} url
 * @property {function} verbose
 * @property {function} [onProgress]
 */

/**
 *
 * @param {module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options} options
 * @constructor
 */
function LoadAndDecode(options) {
	this.url        = options.url;
	this.onProgress = options.onProgress;
	this.verbose    = options.verbose;

	this.context = getAudioContext();
}

LoadAndDecode.prototype = {
	constructor: LoadAndDecode,

	load: function() {
		var request = requestService({
			url: this.url,
			responseType: 'arraybuffer',
			method: 'GET',
			onProgress: this.onProgress,
			verbose: this.verbose
		});

		return request.makeRequest();
	},

	decodeBuffer: function(compressedBuffer) {
		return new Promise(function(fulfill, reject) {
			this.context.decodeAudioData(compressedBuffer, function(buffer) {
					fulfill(buffer);
				}.bind(this), function(error) {
					reject(new Error(error));
				}.bind(this)
			);
		}.bind(this));
	},

	createAudioObject: function(buffer) {
		return {
			buffer: buffer,
			context: this.context
		}
	}
};

/**
 *
 * @param {module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options} options
 * @returns {Promise}
 */
module.exports = function(options) {
	var loadAndDecode = new LoadAndDecode(options);

	return loadAndDecode
		.load()
		.then(loadAndDecode.decodeBuffer.bind(loadAndDecode))
		.then(loadAndDecode.createAudioObject.bind(loadAndDecode));
};