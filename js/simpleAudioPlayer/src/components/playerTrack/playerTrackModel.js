'use strict';

/**
 * This is model of player track which holds information about audio file 
 * and it current time position (with duration)
 * @module simpleAudioPlayer/components/playerTrack/playerTrackModel
 */
	
var loadAndDecode  = require('./playerTrackLoadAndDecode');
var util           = require('../../services/util');

/**
 * 
 * @param {object} options
 * @param {string} options.url url of track (for downloading)
 * @param {function} options.verbose
 * @constructor
 */
function PlayerTrackModel(options) {
	if (typeof options !== 'object') {
		throw new Error('PlayerTrackModel options must be an object, but it\'s ' + util.toString(options));
	}

	if (typeof options.url !== 'string') {
		throw new Error('PlayerTrackModel options.url must be a string, but it\'s ' + util.toString(options.url));
	}

	this.verbose = options.verbose;

	this.url = options.url;
	this.audio = null;
	this.source = null;
	this.currTime = ko.observable(0);
	this.duration = ko.observable(0);

	this.loadAndDecodePromise = null;

	this.startedAt = null;
	this.pausedAt = null;
	this.loadedInPercent = ko.observable(-1);
}

PlayerTrackModel.prototype = {
	constructor: PlayerTrackModel,

	/**
	 * Load audio file using options.url and decode it for later playing
	 */
	loadAndDecode: function() {
		if (!this.loadAndDecodePromise) {
			this.loadAndDecodePromise = loadAndDecode({
				url: this.url,
				onProgress: this.onLoadProgress.bind(this),
				verbose: this.verbose
			}).then(function(audio) {
				this.audio = audio;
				this.duration(audio.buffer.duration);
			}.bind(this));
		}

		return this.loadAndDecodePromise;
	},

	/**
	 * XMLHttpRequest onProgress event callback which calculates already loaded content in %
	 */
	onLoadProgress: function(event) {
		var loaded = Math.round(event.loaded * 100 / event.total);
		this.loadedInPercent(loaded);
	},

	/**
	 * Setup audio chain
	 */
	playPrepare: function() {
		if (!this.audio) {
			return;
		}

		this.source = this.audio.context.createBufferSource();
		this.source.buffer = this.audio.buffer;

		var gainNode = this.audio.context.createGain();
		this.source.connect(gainNode);
		gainNode.connect(this.audio.context.destination);
		this.gainNode = gainNode;
	},

	/**
	 * Begin playing track
	 */
	play: function() {
		if (!this.audio) {
			return;
		}

		this.playPrepare();

		if (typeof this.pausedAt === 'number') {
			this.currTime(this.pausedAt - this.startedAt);
		}
		this.source.start(0, this.currTime());
		this.startedAt = this.audio.context.currentTime - this.currTime();

		this.updateTrackOffsetInterval = setInterval(function() {
			this.currTime(this.audio.context.currentTime - this.startedAt);
		}.bind(this), 1000);
	},

	/**
	 * Change current time of current track to new time
	 * @param {number} newTime
	 */
	changeCurrTimeTo: function(newTime) {
		this.pausedAt = null;
		this.currTime(newTime);
		clearInterval(this.updateTrackOffsetInterval);
	},

	/**
	 * Stop playing track
	 */
	stop: function() {
		clearInterval(this.updateTrackOffsetInterval);

		if (!this.audio || !this.source) {
			return;
		}

		this.pausedAt = this.audio.context.currentTime;

		this.source.stop(0);
		this.source = null;
	},

	/**
	 * Turn track volume to 0%
	 */
	mute: function() {
		if (this.gainNode) {
			this.gainNode.gain.value = 0;
		}
	},

	/**
	 * Turn track volume to 100%
	 */
	unmute: function() {
		if (this.gainNode) {
			this.gainNode.gain.value = 1;
		}
	}
};

module.exports = PlayerTrackModel;