'use strict';

/**
 * Util collection
 * @module simpleAudioPlayer/services/util
 */

/**
 * Returns "[object TypeName]" string
 * @param {*} val
 * @returns {String}
 */
function toString(val) {
	return ({}).toString.call(val);
}

/**
 * Returns true if value is array
 * @param {*} val
 * @returns {boolean}
 */
function isArray(val) {
	return Array.isArray(val);
}

/**
 * Returns true if value is function
 * @param {*} val
 * @returns {boolean}
 */
function isFunction(val) {
	return typeof val === 'function';
}

/**
 * Returns true if value is undefined
 * @param {*} val
 * @returns {boolean}
 */
function isUndefined(val) {
	return typeof val === 'undefined';
}

/**
 * Returns verbose function which make console.log if isActive=true
 * and do nothing if isActive=false
 * Verbose should be created once at app init with isActive flag
 * @param {boolean} isActive
 * @returns {Function}
 */
function createVerboseFunction(isActive) {
	if (window.console && typeof window.console.info === 'function' && isActive) {
		var console = window.console;

		return function() {
			console.info.apply(console, arguments);
		};
	}
	else {
		return function() {};
	}
}

/**
 * Convert seconds to MM:SS-like string
 * @param {number} totalSeconds
 * @returns {string}
 */
function toMMSS(totalSeconds) {
	totalSeconds = ~~totalSeconds;

	var minutes = parseInt( totalSeconds / 60 ) % 60;
	var seconds = totalSeconds % 60;

	if (minutes < 10) {
		minutes = '0' + minutes;
	}

	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	return minutes + ':' + seconds;
}

module.exports = {
	toString:               toString,
	isArray:                isArray,
	isFunction:             isFunction,
	isUndefined:            isUndefined,
	createVerboseFunction:  createVerboseFunction,
	toMMSS:                 toMMSS
};
