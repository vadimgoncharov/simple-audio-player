'use strict';

/**
 * Request maker service
 * @module simpleAudioPlayer/services/request
 */

/**
 * @typedef module:simpleAudioPlayer/services/request~options
 * @type {object} options
 * @property {string} url
 * @property {string} method
 * @property {string} responseType
 * @property {function} verbose
 * @property {function} [onProgress]
 */

/**
 *
 * @param {module:simpleAudioPlayer/services/request~options} options
 * @constructor
 */
function Request(options) {
	this.url = options.url;
	this.method = options.method;
	this.responseType = options.responseType;
	this.onProgress = typeof options.onProgress === 'function' ? options.onProgress : null;
	this.verbose = options.verbose;

	this.promiseFulfill = null;
	this.promiseReject = null;
	this.xhr = null;

	this.promise = new Promise(function(fulfill, reject) {
		this.promiseFulfill = fulfill;
		this.promiseReject = reject;
	}.bind(this));
}

Request.prototype = {
	constructor: Request,

	API: function() {
		return {
			promise:        this.promise,
			abort:          this.abort.bind(this),
			makeRequest:    this.makeRequest.bind(this)
		}
	},

	abort: function() {
		this.xhr.abort();
	},

	makeRequest: function() {
		if (this.xhr) {
			return this.promise;
		}

		var xhr = new XMLHttpRequest();
		xhr.open(this.method, this.url, true);
		xhr.responseType = this.responseType;

		if (typeof this.onProgress === 'function') {
			xhr.addEventListener('progress', this.onProgress, false);
		}

		xhr.onload = function() {
			this.promiseFulfill(xhr.response);
		}.bind(this);

		xhr.onerror = function(event) {
			this.promiseReject(event);
		}.bind(this);

		xhr.send();

		this.xhr = xhr;

		return this.promise;
	}
};

/**
 *
 * @param {module:simpleAudioPlayer/services/request~options} options
 * @returns {*}
 */
module.exports = function(options) {
	var request = new Request(options);
	return request.API();
};
