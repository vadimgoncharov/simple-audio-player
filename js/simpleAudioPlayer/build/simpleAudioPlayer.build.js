(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var MuteButtonTemplate     = require('./muteButtonTpl.html');
var MuteButtonViewModel    = require('./muteButtonViewModel');

function MuteButtonComponent() {}

MuteButtonComponent.prototype = {
	constructor: MuteButtonComponent,
	componentName: 'mute-button',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: MuteButtonViewModel,
				template: MuteButtonTemplate
			});
		}
	}
};

module.exports = new MuteButtonComponent();
},{"./muteButtonTpl.html":2,"./muteButtonViewModel":3}],2:[function(require,module,exports){
module.exports = "<button data-bind=\"\n    click: toggleMute,\n    css: {\n        active: isMute()\n    }\" type=\"button\" class=\"btn btn-default btn-xs\" aria-label=\"Left Align\">\n    <span data-bind=\"css: {\n        'glyphicon-volume-up': !isMute(),\n        'glyphicon-volume-off': isMute()\n    }\" class=\"glyphicon\" aria-hidden=\"true\"></span>\n</button>";

},{}],3:[function(require,module,exports){
'use strict';

/**
 * @module simpleAudioPlayer/components/ko/muteButton/muteButtonViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function MuteButtonViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.isMute = this.playerViewModel.isMute;
}

MuteButtonViewModel.prototype = {
	constructor: MuteButtonViewModel,

	toggleMute: function() {
		this.isMute(!this.isMute());
	}
};

module.exports = MuteButtonViewModel;
},{}],4:[function(require,module,exports){
'use strict';

var PlayPauseButtonTemplate     = require('./playPauseButtonTpl.html');
var PlayPauseButtonViewModel    = require('./playPauseButtonViewModel');

function PlayPauseButtonComponent() {}

PlayPauseButtonComponent.prototype = {
	constructor: PlayPauseButtonComponent,
	componentName: 'play-pause-button',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: PlayPauseButtonViewModel,
				template: PlayPauseButtonTemplate
			});
		}
	}
};

module.exports = new PlayPauseButtonComponent();
},{"./playPauseButtonTpl.html":5,"./playPauseButtonViewModel":6}],5:[function(require,module,exports){
module.exports = "<button data-bind=\"click: togglePlay\" type=\"button\" class=\"btn btn-default btn-xs\" aria-label=\"Left Align\">\n    <span data-bind=\"css: {\n        'glyphicon-play': !isPlaying(),\n        'glyphicon-pause': isPlaying()\n    }\" class=\"glyphicon\" aria-hidden=\"true\" ></span>\n</button>";

},{}],6:[function(require,module,exports){
'use strict';

/**
 * @module simpleAudioPlayer/components/ko/playPauseButton/playPauseButtonViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function PlayPauseButtonViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.isPlaying = this.playerViewModel.isPlaying;
}

PlayPauseButtonViewModel.prototype = {
	constructor: PlayPauseButtonViewModel,

	togglePlay: function() {
		this.isPlaying(!this.isPlaying());
	}
};

module.exports = PlayPauseButtonViewModel;
},{}],7:[function(require,module,exports){
'use strict';

var TimeCounterTemplate     = require('./timeCounterTpl.html');
var TimeCounterViewModel    = require('./timeCounterViewModel');

function TimelineComponent() {}

TimelineComponent.prototype = {
	constructor: TimelineComponent,
	componentName: 'time-counter',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: TimeCounterViewModel,
				template: TimeCounterTemplate
			});
		}
	}
};

module.exports = new TimelineComponent();
},{"./timeCounterTpl.html":8,"./timeCounterViewModel":9}],8:[function(require,module,exports){
module.exports = "<div class=\"simple-player__time-counter\">\n    <span data-bind=\"text: trackCurrTimeInMMSS()\"></span> / <span data-bind=\"text: trackDurationInMMSS()\"></span>\n</div>";

},{}],9:[function(require,module,exports){
'use strict';

/**
 * @module simpleAudioPlayer/components/ko/timeCounter/timeCounterViewModel
 */

var util = require('../../../services/util');

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function TimeCounterViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.trackCurrTime = this.playerViewModel.trackCurrTime;
	this.trackDuration = this.playerViewModel.trackDuration;

	this.trackCurrTimeInMMSS = ko.computed(function() {
		return util.toMMSS(this.trackCurrTime());
	}, this);

	this.trackDurationInMMSS = ko.computed(function() {
		return util.toMMSS(this.trackDuration());
	}, this);

}

module.exports = TimeCounterViewModel;
},{"../../../services/util":19}],10:[function(require,module,exports){
'use strict';

var TimeLineTemplate     = require('./timeLineTpl.html');
var TimeLineViewModel    = require('./timeLineViewModel');

function TimelineComponent() {}

TimelineComponent.prototype = {
	constructor: TimelineComponent,
	componentName: 'time-line',

	register: function() {
		if (!ko.components.isRegistered(this.componentName)) {
			ko.components.register(this.componentName, {
				viewModel: TimeLineViewModel,
				template: TimeLineTemplate
			});
		}
	}
};

module.exports = new TimelineComponent();
},{"./timeLineTpl.html":11,"./timeLineViewModel":12}],11:[function(require,module,exports){
module.exports = "\n<div data-bind=\"css: {'simple-player__range_in-progress_yes': loadedInPercent() >=0 && loadedInPercent() < 100}\" class=\"simple-player__range\">\n\n    <input\n        style=\"width: 1%\"\n        type=\"range\"\n        class=\"simple-player__range-input\"\n        min=\"0\"\n        step=\"1\"\n        value=\"0\"\n        data-bind=\"\n            style: {width: inputWidth()},\n            value: trackCurrTime,\n            attr: {value: trackCurrTime(), max: trackDuration()},\n            event: {change: onRangeChange, input: onRangeInput}\"\n    />\n    <div class=\"simple-player__range-line\"></div>\n</div>";

},{}],12:[function(require,module,exports){
'use strict';

/**
 * @module simpleAudioPlayer/components/ko/timeLine/timeLineViewModel
 */

/**
 *
 * @param {object} params
 * @param {PlayerViewModel} params.playerViewModel
 * @constructor
 */
function TimeLineViewModel(params) {
	this.playerViewModel = params.playerViewModel;

	this.trackCurrTime = ko.computed(function() {
		return Math.round(this.playerViewModel.trackCurrTime());
	}, this);

	this.trackDuration = ko.computed(function() {
		return Math.round(this.playerViewModel.trackDuration());
	}, this);
	
	this.playingValueOnStartRangeInput = null;
	this.loadedInPercent = this.playerViewModel.loadedInPercent;

	this.inputWidth = ko.computed(function() {
		return this.loadedInPercent() + '%';
	}, this);

}

TimeLineViewModel.prototype = {
	constructor: TimeLineViewModel,

	onRangeChange: function(data, event) {
		this.playerViewModel.changeCurrTrackTimeTo(event.target.value);

		if (this.playingValueOnStartRangeInput !== null) {
			this.playerViewModel.isPlaying(this.playingValueOnStartRangeInput);
			this.playingValueOnStartRangeInput = null;
		}
	},

	onRangeInput: function(data, event) {
		if (this.playingValueOnStartRangeInput === null) {
			this.playingValueOnStartRangeInput = this.playerViewModel.isPlaying();
		}
		this.playerViewModel.changeCurrTrackTimeTo(event.target.value);
	}
};

module.exports = TimeLineViewModel;
},{}],13:[function(require,module,exports){
'use strict';

/**
 * This is main app component which creates child components
 * @module simpleAudioPlayer/components/player/playerController
 */

var PlayerViewModel    = require('./playerViewModel');
var PlayerTpl          = require('./playerTpl.html');

var PlayPauseButtonComponent    = require('../ko/playPauseButton/playPauseButtonComponent');
var MuteButtonComponent         = require('../ko/muteButton/muteButtonComponent');
var TimeLineComponent           = require('../ko/timeLine/timeLineComponent');
var TimeCounterComponent        = require('../ko/timeCounter/timeCounterComponent');

var PlayerTrackModel            = require('../playerTrack/playerTrackModel');
var util                        = require('../../services/util');

function PlayerController(rootEl, options) {
	this.rootEl = rootEl;
	this.options = options;

	this.createVerbose();
	this.registerKoComponents();
	this.initPlayer();
	this.renderPlayer();
}

PlayerController.prototype = {
	constructor: PlayerController,

	createVerbose: function() {
		this.verbose = util.createVerboseFunction(this.options.verbose);
	},

	registerKoComponents: function() {
		PlayPauseButtonComponent.register();
		MuteButtonComponent.register();
		TimeLineComponent.register();
		TimeCounterComponent.register();
	},

	initPlayer: function() {
		this.playerViewModel = new PlayerViewModel({
			verbose: this.verbose
		});

		var playerTrackModel = new PlayerTrackModel({
			url: this.options.trackUrl,
			verbose: this.verbose
		});

		this.playerViewModel.setTrack(playerTrackModel);
	},

	renderPlayer: function() {
		var el = document.createElement('div');
		el.innerHTML = PlayerTpl;
		this.rootEl.appendChild(el);

		ko.applyBindings(this.playerViewModel, this.rootEl);
	}
};

module.exports = PlayerController;

},{"../../services/util":19,"../ko/muteButton/muteButtonComponent":1,"../ko/playPauseButton/playPauseButtonComponent":4,"../ko/timeCounter/timeCounterComponent":7,"../ko/timeLine/timeLineComponent":10,"../playerTrack/playerTrackModel":17,"./playerTpl.html":14,"./playerViewModel":15}],14:[function(require,module,exports){
module.exports = "<div class=\"simple-player\">\n    <div class=\"simple-player__layout\">\n        <div class=\"simple-player__layout-col simple-player__layout-col_name_play-pause\">\n            <!-- ko component: {\n                name: \"play-pause-button\",\n                params: { playerViewModel: playerViewModel}\n            } -->\n            <!-- /ko -->\n        </div>\n\n        <div class=\"simple-player__layout-col simple-player__layout-col_name_time-line\">\n            <!-- ko component: {\n                name: \"time-line\",\n                params: { playerViewModel: playerViewModel}\n            } -->\n            <!-- /ko -->\n        </div>\n\n        <div class=\"simple-player__layout-col simple-player__layout-col_name_time-counter\">\n            <!-- ko component: {\n                name: \"time-counter\",\n                params: { playerViewModel: playerViewModel}\n            } -->\n            <!-- /ko -->\n        </div>\n\n        <div class=\"simple-player__layout-col simple-player__layout-col_name_volume\">\n            <!-- ko component: {\n                name: \"mute-button\",\n                params: { playerViewModel: playerViewModel}\n            } -->\n            <!-- /ko -->\n        </div>\n    </div>\n</div>";

},{}],15:[function(require,module,exports){
'use strict';

/**
 * @module simpleAudioPlayer/components/player/playerViewModel
 */

/**
 *
 * @param {object} options
 * @param {function} options.verbose
 * @constructor
 */
function PlayerViewModel(options) {
	this.verbose = options.verbose;

	this.playerViewModel = this;
	this.playerTrackModel = null;
	this.trackCurrTime = ko.observable(0);
	this.trackDuration = ko.observable(0);
	this.isPlaying = ko.observable(false);
	this.isMute = ko.observable(false);
	this.loadedInPercent = ko.observable(-1);

	this.isPlaying.subscribe(this.changePlayingByIsPlaying, this);
	this.isMute.subscribe(this.changeMuteByIsMute, this);
	this.trackCurrTime.subscribe(this.stopIfReachEnd, this);
}

PlayerViewModel.prototype = {
	constructor: PlayerViewModel,

	/**
	 * Reset timer to beginning if it reachs the end
	 */
	stopIfReachEnd: function() {
		if (this.trackCurrTime() > this.trackDuration()) {
			this.isPlaying(false);
			this.trackCurrTime(this.trackDuration());
		}
	},

	/**
	 * Change Player playing state by testing isPlaying prop
	 */
	changePlayingByIsPlaying: function() {
		if (this.isPlaying()) {
			this.play();
		}
		else {
			this.stop();
		}
	},

	/**
	 * Change Player mute state by testing isMute prop
	 */
	changeMuteByIsMute: function() {
		if (this.isMute()) {
			this.playerTrackModel.mute();
		}
		else {
			this.playerTrackModel.unmute();
		}
	},

	/**
	 * Set current active track
	 * @param {PlayerTrackModel} playerTrackModel
	 */
	setTrack: function(playerTrackModel) {
		this.playerTrackModel = playerTrackModel;

		this.playerTrackModel.currTime.subscribe(this.trackCurrTime, this);
		this.playerTrackModel.duration.subscribe(this.trackDuration, this);
		this.playerTrackModel.loadedInPercent.subscribe(this.loadedInPercent, this);
	},

	/**
	 * Change current time of current track to new time
	 * @param {number} newTime
	 */
	changeCurrTrackTimeTo: function(newTime) {
		this.isPlaying(false);
		this.playerTrackModel.changeCurrTimeTo(newTime);
	},

	/**
	 * Run audio track
	 */
	play: function() {
		if (this.trackCurrTime() >= this.trackDuration()) {
			this.playerTrackModel.changeCurrTimeTo(0);
		}
		this.playerTrackModel.loadAndDecode()
			.then(function() {
				this.playerTrackModel.play();
				this.changeMuteByIsMute();
			}.bind(this))
			.catch(function(error) {
				console.error(error);
			}
		);
	},

	/**
	 * Stop audio track
	 */
	stop: function() {
		this.playerTrackModel.stop();
	}
};

module.exports = PlayerViewModel;

},{}],16:[function(require,module,exports){
'use strict';

/**
 * Load track using XMLHttpRequest and decodes compressedBuffer to buffer
 * @module simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode
 */

var requestService = require('../../services/request');

var getAudioContext = (function() {
	var AudioContext;

	return function() {
		if (!AudioContext) {
			try {
				AudioContext = window.AudioContext || window.webkitAudioContext;
			}
			catch (error) {
				throw new Error('Web Audio API is not supported in this browser');
			}
		}

		return new AudioContext;
	}
})();

/**
 * @typedef module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options
 * @type {object} options
 * @property {string} url
 * @property {function} verbose
 * @property {function} [onProgress]
 */

/**
 *
 * @param {module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options} options
 * @constructor
 */
function LoadAndDecode(options) {
	this.url        = options.url;
	this.onProgress = options.onProgress;
	this.verbose    = options.verbose;

	this.context = getAudioContext();
}

LoadAndDecode.prototype = {
	constructor: LoadAndDecode,

	load: function() {
		var request = requestService({
			url: this.url,
			responseType: 'arraybuffer',
			method: 'GET',
			onProgress: this.onProgress,
			verbose: this.verbose
		});

		return request.makeRequest();
	},

	decodeBuffer: function(compressedBuffer) {
		return new Promise(function(fulfill, reject) {
			this.context.decodeAudioData(compressedBuffer, function(buffer) {
					fulfill(buffer);
				}.bind(this), function(error) {
					reject(new Error(error));
				}.bind(this)
			);
		}.bind(this));
	},

	createAudioObject: function(buffer) {
		return {
			buffer: buffer,
			context: this.context
		}
	}
};

/**
 *
 * @param {module:simpleAudioPlayer/components/playerTrack/playerTrackLoadAndDecode~options} options
 * @returns {Promise}
 */
module.exports = function(options) {
	var loadAndDecode = new LoadAndDecode(options);

	return loadAndDecode
		.load()
		.then(loadAndDecode.decodeBuffer.bind(loadAndDecode))
		.then(loadAndDecode.createAudioObject.bind(loadAndDecode));
};
},{"../../services/request":18}],17:[function(require,module,exports){
'use strict';

/**
 * This is model of player track which holds information about audio file 
 * and it current time position (with duration)
 * @module simpleAudioPlayer/components/playerTrack/playerTrackModel
 */
	
var loadAndDecode  = require('./playerTrackLoadAndDecode');
var util           = require('../../services/util');

/**
 * 
 * @param {object} options
 * @param {string} options.url url of track (for downloading)
 * @param {function} options.verbose
 * @constructor
 */
function PlayerTrackModel(options) {
	if (typeof options !== 'object') {
		throw new Error('PlayerTrackModel options must be an object, but it\'s ' + util.toString(options));
	}

	if (typeof options.url !== 'string') {
		throw new Error('PlayerTrackModel options.url must be a string, but it\'s ' + util.toString(options.url));
	}

	this.verbose = options.verbose;

	this.url = options.url;
	this.audio = null;
	this.source = null;
	this.currTime = ko.observable(0);
	this.duration = ko.observable(0);

	this.loadAndDecodePromise = null;

	this.startedAt = null;
	this.pausedAt = null;
	this.loadedInPercent = ko.observable(-1);
}

PlayerTrackModel.prototype = {
	constructor: PlayerTrackModel,

	/**
	 * Load audio file using options.url and decode it for later playing
	 */
	loadAndDecode: function() {
		if (!this.loadAndDecodePromise) {
			this.loadAndDecodePromise = loadAndDecode({
				url: this.url,
				onProgress: this.onLoadProgress.bind(this),
				verbose: this.verbose
			}).then(function(audio) {
				this.audio = audio;
				this.duration(audio.buffer.duration);
			}.bind(this));
		}

		return this.loadAndDecodePromise;
	},

	/**
	 * XMLHttpRequest onProgress event callback which calculates already loaded content in %
	 */
	onLoadProgress: function(event) {
		var loaded = Math.round(event.loaded * 100 / event.total);
		this.loadedInPercent(loaded);
	},

	/**
	 * Setup audio chain
	 */
	playPrepare: function() {
		if (!this.audio) {
			return;
		}

		this.source = this.audio.context.createBufferSource();
		this.source.buffer = this.audio.buffer;

		var gainNode = this.audio.context.createGain();
		this.source.connect(gainNode);
		gainNode.connect(this.audio.context.destination);
		this.gainNode = gainNode;
	},

	/**
	 * Begin playing track
	 */
	play: function() {
		if (!this.audio) {
			return;
		}

		this.playPrepare();

		if (typeof this.pausedAt === 'number') {
			this.currTime(this.pausedAt - this.startedAt);
		}
		this.source.start(0, this.currTime());
		this.startedAt = this.audio.context.currentTime - this.currTime();

		this.updateTrackOffsetInterval = setInterval(function() {
			this.currTime(this.audio.context.currentTime - this.startedAt);
		}.bind(this), 1000);
	},

	/**
	 * Change current time of current track to new time
	 * @param {number} newTime
	 */
	changeCurrTimeTo: function(newTime) {
		this.pausedAt = null;
		this.currTime(newTime);
		clearInterval(this.updateTrackOffsetInterval);
	},

	/**
	 * Stop playing track
	 */
	stop: function() {
		clearInterval(this.updateTrackOffsetInterval);

		if (!this.audio || !this.source) {
			return;
		}

		this.pausedAt = this.audio.context.currentTime;

		this.source.stop(0);
		this.source = null;
	},

	/**
	 * Turn track volume to 0%
	 */
	mute: function() {
		if (this.gainNode) {
			this.gainNode.gain.value = 0;
		}
	},

	/**
	 * Turn track volume to 100%
	 */
	unmute: function() {
		if (this.gainNode) {
			this.gainNode.gain.value = 1;
		}
	}
};

module.exports = PlayerTrackModel;
},{"../../services/util":19,"./playerTrackLoadAndDecode":16}],18:[function(require,module,exports){
'use strict';

/**
 * Request maker service
 * @module simpleAudioPlayer/services/request
 */

/**
 * @typedef module:simpleAudioPlayer/services/request~options
 * @type {object} options
 * @property {string} url
 * @property {string} method
 * @property {string} responseType
 * @property {function} verbose
 * @property {function} [onProgress]
 */

/**
 *
 * @param {module:simpleAudioPlayer/services/request~options} options
 * @constructor
 */
function Request(options) {
	this.url = options.url;
	this.method = options.method;
	this.responseType = options.responseType;
	this.onProgress = typeof options.onProgress === 'function' ? options.onProgress : null;
	this.verbose = options.verbose;

	this.promiseFulfill = null;
	this.promiseReject = null;
	this.xhr = null;

	this.promise = new Promise(function(fulfill, reject) {
		this.promiseFulfill = fulfill;
		this.promiseReject = reject;
	}.bind(this));
}

Request.prototype = {
	constructor: Request,

	API: function() {
		return {
			promise:        this.promise,
			abort:          this.abort.bind(this),
			makeRequest:    this.makeRequest.bind(this)
		}
	},

	abort: function() {
		this.xhr.abort();
	},

	makeRequest: function() {
		if (this.xhr) {
			return this.promise;
		}

		var xhr = new XMLHttpRequest();
		xhr.open(this.method, this.url, true);
		xhr.responseType = this.responseType;

		if (typeof this.onProgress === 'function') {
			xhr.addEventListener('progress', this.onProgress, false);
		}

		xhr.onload = function() {
			this.promiseFulfill(xhr.response);
		}.bind(this);

		xhr.onerror = function(event) {
			this.promiseReject(event);
		}.bind(this);

		xhr.send();

		this.xhr = xhr;

		return this.promise;
	}
};

/**
 *
 * @param {module:simpleAudioPlayer/services/request~options} options
 * @returns {*}
 */
module.exports = function(options) {
	var request = new Request(options);
	return request.API();
};

},{}],19:[function(require,module,exports){
'use strict';

/**
 * Util collection
 * @module simpleAudioPlayer/services/util
 */

/**
 * Returns "[object TypeName]" string
 * @param {*} val
 * @returns {String}
 */
function toString(val) {
	return ({}).toString.call(val);
}

/**
 * Returns true if value is array
 * @param {*} val
 * @returns {boolean}
 */
function isArray(val) {
	return Array.isArray(val);
}

/**
 * Returns true if value is function
 * @param {*} val
 * @returns {boolean}
 */
function isFunction(val) {
	return typeof val === 'function';
}

/**
 * Returns true if value is undefined
 * @param {*} val
 * @returns {boolean}
 */
function isUndefined(val) {
	return typeof val === 'undefined';
}

/**
 * Returns verbose function which make console.log if isActive=true
 * and do nothing if isActive=false
 * Verbose should be created once at app init with isActive flag
 * @param {boolean} isActive
 * @returns {Function}
 */
function createVerboseFunction(isActive) {
	if (window.console && typeof window.console.info === 'function' && isActive) {
		var console = window.console;

		return function() {
			console.info.apply(console, arguments);
		};
	}
	else {
		return function() {};
	}
}

/**
 * Convert seconds to MM:SS-like string
 * @param {number} totalSeconds
 * @returns {string}
 */
function toMMSS(totalSeconds) {
	totalSeconds = ~~totalSeconds;

	var minutes = parseInt( totalSeconds / 60 ) % 60;
	var seconds = totalSeconds % 60;

	if (minutes < 10) {
		minutes = '0' + minutes;
	}

	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	return minutes + ':' + seconds;
}

module.exports = {
	toString:               toString,
	isArray:                isArray,
	isFunction:             isFunction,
	isUndefined:            isUndefined,
	createVerboseFunction:  createVerboseFunction,
	toMMSS:                 toMMSS
};

},{}],20:[function(require,module,exports){
'use strict';

var PlayerController = require('./components/player/playerController');

window.SimpleAudioPlayer = {
	createInstance: function(rootEl, options) {
		var pc = new PlayerController(rootEl, options);
	}
};
},{"./components/player/playerController":13}]},{},[20]);
